const fs = require("fs");
const path = require("path");
const absolutePath = "/home/aryan-bisht/Desktop/vs workspace/async-drill";
let allFiles = [];

function readTheFile(absolutePath, callback) {
  allFiles.forEach((file) => {
    const location = path.join(absolutePath, file);
    fs.readFile(location, "utf8", (err, data) => {
      if (err) {
        console.error("Error reading file:", err);
        callback(err, null);
        return;
      }
      console.log(data);
      callback(null, data);
    });
  });
}

function sorted(absolutePath, callback) {
  const location = path.join(absolutePath, "sorted.txt");
  allFiles.push("sorted.txt");
  const fileLocation = path.join(absolutePath, "lower.txt");
  fs.readFile(fileLocation, "utf8", (err, data) => {
    if (err) {
      console.error("Error reading file:", err);
      callback(err, null);
      return;
    }
    const newData = data.split(" ").sort().join(" ");
    fs.writeFile(location, newData, (err) => {
      if (err) {
        console.error("Error writing file:", err);
        callback(err, null);
        return;
      }
      callback(null, newData);
    });
  });
}

function toLower(absolutePath, callback) {
  const location = path.join(absolutePath, "lower.txt");
  const fileLocation = path.join(absolutePath, "upper.txt");
  allFiles.push("lower.txt");
  fs.readFile(fileLocation, "utf8", (err, data) => {
    if (err) {
      console.error("Error reading file:", err);
      callback(err, null);
      return;
    }
    const newData = data.toLowerCase().split(".").join("\n");
    fs.writeFile(location, newData, (err) => {
      if (err) {
        console.error("Error writing file:", err);
        callback(err, null);
        return;
      }
      callback(null, newData);
    });
  });
}

function toUpper(absolutePath, callback) {
  const fileLocation = path.join(absolutePath, "upper.txt");
  allFiles.push("upper.txt");
  const location = path.join(absolutePath, "lipsum_1.txt");
  fs.readFile(location, (err, data) => {
    if (err) {
      console.error("Error reading file:", err);
      callback(err, null);
      return;
    }

    const newData = data.toString().toUpperCase();

    fs.writeFile(fileLocation, newData, (err) => {
      if (err) {
        console.error("Error writing file:", err);
        callback(err, null);
        return;
      }
      callback(null, newData);
    });
  });
}

function deleteFiles(absolutePath, callback) {
  allFiles.forEach((files) => {
    let fileName = allFiles.pop();
    const fileLocation = path.join(absolutePath, fileName);
    fs.unlink(fileLocation, (err) => {
      if (err) {
        console.error("Error deleting file:", err);
      } else {
        console.log("File deleted successfully:", fileLocation);
      }
    });
  });
}

function problem2(absolutePath) {
  toUpper(absolutePath, () => {
    toLower(absolutePath, () => {
      sorted(absolutePath, () => {
        readTheFile(absolutePath, () => {
          deleteFiles(absolutePath, () => {
            console.log("All operations completed successfully.");
          });
        });
      });
    });
  });
}

module.exports = problem2;
