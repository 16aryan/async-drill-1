const fs = require("fs");
const path = require("path");
const absolutePath = "/home/aryan-bisht/Desktop/vs workspace/async-drill";

function creatingFiles(absolutePath, randomNumber, callback) {
  let filesCreated = 0;
  for (let index = 1; index <= randomNumber; index++) {
    let file = `file${index}.json`;
    let filePath = path.join(absolutePath, file);

    fs.writeFile(filePath, `file${index}`, function (err) {
      if (err) {
        callback(err);
        return;
      }
      console.log(`Saved ${file}!`);
      filesCreated++;
      if (filesCreated === randomNumber) {
        callback(null);
      }
    });
  }
}

function deletingFiles(absolutePath, randomNumber, callback) {
  let filesDeleted = 0;
  for (let index = 1; index <= randomNumber; index++) {
    let file = `file${index}.json`;
    let filePath = path.join(absolutePath, file);

    fs.unlink(filePath, function (err) {
      if (err) {
        console.error(`Error deleting ${file}:`, err);
        callback(err);
        return;
      }
      console.log(`Deleted ${file}!`);
      filesDeleted++;
      if (filesDeleted === randomNumber) {
        callback(null);
      }
    });
  }
}

function problem1(absolutePath, randomNumber) {
  fs.mkdir(path.join(__dirname, "output"), (err) => {
    if (err) {
      return console.error(err);
    }
    console.log("Directory created successfully!");

    creatingFiles(absolutePath, randomNumber, (err) => {
      if (err) {
        console.error("Error creating files:", err);
        return;
      }
      deletingFiles(absolutePath, randomNumber, (err) => {
        if (err) {
          console.error("Error deleting files:", err);
          return;
        }
        console.log("Task completed.");
        fs.rmdir(path.join(__dirname, "output"), (err) => {
          if (err) {
            throw err;
          }

          console.log(`deleted folder is deleted!`);
        });
      });
    });
  });
}
module.exports = problem1;
